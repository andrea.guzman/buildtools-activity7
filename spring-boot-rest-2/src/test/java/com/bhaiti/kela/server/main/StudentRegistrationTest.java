package com.bhaiti.kela.server.main;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.bhaiti.kela.beans.Student;
import com.bhaiti.kela.beans.StudentRegistration;
import org.junit.Test;

public class StudentRegistrationTest {
    
    @Test
	public void getInstanceNotNull() {
        assertTrue(StudentRegistration.getInstance() != null); 
	}
    
    @Test
	public void upDateStudentTest() {
        Student nuevo = new Student("Monse", 34, "12345");
        assertEquals("Update un-successful", StudentRegistration.upDateStudent(nuevo));	
    }
    
    @Test(expected = NullPointerException.class)
    public void deleteStudentTest() {
        assertEquals("Delete un-successful", StudentRegistration.deleteStudent("12345"));
	}

}
